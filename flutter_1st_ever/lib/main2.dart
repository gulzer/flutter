import "package:flutter/material.dart";
import 'app_screens/1st_screen.dart';

void main() => runApp(new myFulutterApp());  //Function Expression instead of curly braces




class myFulutterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return    new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "1st ever",
        home: Scaffold(
            appBar: AppBar(title: Text ("My app name bar",
              style: TextStyle(color: Colors.orange, fontSize:  20.0),
              ),
              ),
            body: firstScreen()
        )
    );
  }
}
