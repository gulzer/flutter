import 'package:flutter/material.dart';

void main(){
  runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Scoring Game",
        home: FavouriteCity(),
      )
  );
}

class FavouriteCity extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FavouriteCityState();
  }
}

class FavouriteCityState extends State<FavouriteCity>{
  String cityName = "";
  var currencies = ["Taka","rupees","Dollar","Pound"];
  var selectedcurrency = "Dollar";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text("Scoring Game"),
        backgroundColor: Colors.purpleAccent,
      ),
      body:  Container(
          margin: EdgeInsets.all(20.1),
          child:Column(
            children: <Widget>[
              TextField(
                onChanged: (String userInput){ //Use 'onChanged' to change the widget in real time
                  setState(() {
                    cityName = userInput;
                  });
                },
              ),

              DropdownButton<String>(
                items: currencies.map((String dropDownStringItem){
                  return DropdownMenuItem<String>(
                    value: dropDownStringItem,
                    child: Text(dropDownStringItem),
                  );
                }).toList(),
                onChanged: (String newValueSelected){
                  // your code will be here
                  setState(() {
                    this.selectedcurrency = newValueSelected;
                  });
                },
              value: selectedcurrency,
              ),

              Padding(
                  padding: EdgeInsets.all(20.2),
                  child: Text(
                    "Your City is $cityName",
                    style: TextStyle(fontSize: 20.0),
                  )
              )
            ],
          )
      ),
    );
  }
}
