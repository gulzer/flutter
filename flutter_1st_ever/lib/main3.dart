import 'dart:developer';
import 'package:flutter/material.dart';
import 'app_screens/home.dart';

void main(){
  runApp(
      MaterialApp(
//        debugShowCheckedModeBanner: false,
        title: "First Ever",
        home:   Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blueGrey,
            title: Text("This is Long List",
              style: (
                  TextStyle(
                      color: Colors.black87)
              ),
            ),
          ),
          body: getLongList(),
          floatingActionButton: FloatingActionButton(
            onPressed: (){
              debugPrint("Clicked to add items");
            },
            child: Icon(Icons.add_box),
            tooltip: "Add one more item",
          ),
        ),
      )
  );
}


//Long List

void showSnackBar(BuildContext context, String item){
  var snackBar= SnackBar(
    content: Text("You just clicked on $item"),
    action: SnackBarAction(
      label: "Undo",
      onPressed: (){
        debugPrint("Action undone");
      },
    ),
  );
  Scaffold.of(context).showSnackBar(snackBar);
}


//Long List
List<String> getListElement(){
  var items = List<String>.generate(1000, (Counter) => "Item no. $Counter");
  return items;
}

Widget getLongList(){
  var listItems = getListElement();
  var listview = ListView.builder(
      itemBuilder: (context,index){
        return ListTile(
          leading: Icon(Icons.wallpaper),
          title: Text (listItems[index]),
          onTap: (){
            debugPrint("${listItems[index]} was tapped");
//            var alertDialog = AlertDialog(
//              title: Text("${listItems[index]} was tapped"),
//            );
//
//            showDialog(
//                context: context,
//                builder: (BuildContext context) => alertDialog   // actually "return  alertDialog;"
//            );
            showSnackBar(context, listItems[index]);
          },
        );
      }
  );
  return listview;
}


// Basic List


Widget getListView(){
  var listview = ListView(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.watch),
          title: Text("Watch Movies"),
          subtitle: Text("Movie Names"),
          trailing: Icon(Icons.wb_sunny),
          onTap: (){
            debugPrint("Landscape tapped");
          },
        ),
        Text("  Yo YO!"),
        Container(color: Colors.deepOrange,height: 50.0)
      ]
  );
  return listview;
}
