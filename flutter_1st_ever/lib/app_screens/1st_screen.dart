import 'dart:math';
import 'package:flutter/material.dart';

class firstScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Material(
      color: Colors.lightBlueAccent,
      child:
      Center(
        child: Text(
//          "Hello Duranta, your lucky number is ${generateLuckyNumber()}", // when it was an integer number and its a string
          generateLuckyNumber(),
          textDirection:TextDirection.ltr,
          style: TextStyle(color: Colors.deepOrange, fontSize: 30.0),
        ),
      ),
    );
  }

  String generateLuckyNumber() { // 1st it was an integer type
    var random = Random();
    int luckyNumber = random.nextInt(50);
//    return luckyNumber;
    return "Hello Duranta, your lucky number is $luckyNumber";
  }
}