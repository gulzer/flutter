import 'dart:ffi';
import 'dart:math';
import 'package:flutter/material.dart';

void main(){
  runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Scoring Game",
        home: FavouriteCity(),
      )
  );
}

class FavouriteCity extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FavouriteCityState();
  }
}

class FavouriteCityState extends State<FavouriteCity>{
  var button1 = randomNum1();
  var button2 = randomNum2();
  var score = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text("Scoring Game"),
        backgroundColor: Colors.teal,
      ),
      body:  Container(
          margin: EdgeInsets.all(20.0),
          child:Column(
            children: <Widget>[
              Text("Lets Play Button Game", style: TextStyle(color: Colors.indigo,fontSize: 40.0),),
              Container(height: 200.0,),
              
              Row(
                children: <Widget>[
                  RaisedButton(
                    color: Colors.deepOrangeAccent,
                    child: Text("$button1", style: TextStyle(fontSize: 20.0)),
                    elevation: 20.0,
                    onPressed: (){
                      setState(() {
                        if(button1 > button2){
                          score++;
                        }
                        else score--;
                        button1 = randomNum1();
                        button2 = randomNum2();
                      }
                      );
                    },
                  ),
                  Container(width: 180.0),
                  RaisedButton(
                    color: Colors.deepOrangeAccent,
                    child: Text("$button2", style: TextStyle(fontSize: 20.0)),
                    elevation: 20.0,
                    onPressed: (){
                      setState(() {
                        if(button2 > button1){
                          score++;
                        }
                        else score--;
                        button1 = randomNum1();
                        button2 = randomNum2();
                      }
                      );
                    },
                  )
                ],
              ),
              Container(height: 200.0,),
              Text("Your Score is $score", style: TextStyle(color: Colors.black45,fontSize: 40.0),)
            ],
          )
      ),
    );
  }
}

randomNum1() { // 1st it was an integer type
  var random = Random();
  int luckyNumber = random.nextInt(1000);
  return luckyNumber;
}
randomNum2() { // 1st it was an integer type
  var random = Random();
  int luckyNumber = random.nextInt(1000);
  return luckyNumber;
}
