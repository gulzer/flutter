import 'package:flutter/material.dart';
import 'package:notifier/notifier.dart';
import 'package:notifier/notifier_provider.dart';
import 'package:notifier/notifier_data.dart';

void main(){
  runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Scoring Game",
        home: NotifierProvider(
          child: broadxast(),
        ) ,
      )

  );
}

class MyApp {
}

class ShowBroadcast extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Text('Data from Notifier:'),
        Notifier.of(context).register<String>('action', (data) {
          return Text('broadcast');
        }),
      ],
    );
  }

}


class broadxast extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    Notifier _notifier = NotifierProvider.of(context);
    return Scaffold(
      body: Center(
        child :RaisedButton(
          child: Text('Notify'),
          onPressed: () {
            _notifier.notify('action', ShowBroadcast());
          },
        ),
      ),
    );
  }

}