import 'dart:math';
import 'package:flutter/material.dart';

void main(){
  runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Scoring Game",
        home: FavouriteCity(),
      )
  );
}

class FavouriteCity extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FavouriteCityState();
  }
}

class FavouriteCityState extends State<FavouriteCity>{
  var button1 = randomNum();
  var button2 = randomNum();
  var score = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text("Scoring Game"),
        backgroundColor: Colors.purpleAccent,
      ),
      body:  Container(
          margin: EdgeInsets.all(20.1),
          child:Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  RaisedButton(
                    color: Colors.tealAccent,
                    child: Text("Value is $button1"),
                  ),
                  RaisedButton(
                    color: Colors.teal[700],
                    child: Text("Value is $button2"),
                  )
                ],
              ),
              Text("Your Score is $score")
            ],
          )
      ),
    );
  }
}

String randomNum() { // 1st it was an integer type
  var random = Random();
  int luckyNumber = random.nextInt(1000);
  return "$luckyNumber";
}
